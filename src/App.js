import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Delivery from './components/Delivery';
import Footer from './components/Footer';
import Header from './components/Header';
import List from './components/List';
import Menu from './components/Menu';
import Products from './components/Products';


function ProductsContainer(props) {
  if (props.app.state.page === 'products') {
      return (<Products
        app={props.app}
        items={props.app.state.items}
        productSelectedAction={props.app.productSelectedAction}
        
      />)
  }
  else return('');
}


class App extends Component {
  constructor(props) {
  	super(props);
  	this.state = {
  		state: 'onboarding',
      page: 'list',
  		suggest: true,
      canClear: false,
      canOrder: false,
      items: [],
      productsSelected: [],
      selectedPrice: 0,
  	};
  	this.addItemAction = this.addItemAction.bind(this);
  	this.hideSuggestAction = this.hideSuggestAction.bind(this);
    this.showMenuAction = this.showMenuAction.bind(this);
    this.closeMenuAction = this.closeMenuAction.bind(this);
    this.clearCheckedAction = this.clearCheckedAction.bind(this);
    this.editStartAction = this.editStartAction.bind(this);
    this.editStopAction = this.editStopAction.bind(this);
  	this.hideNewItem = this.hideNewItem.bind(this);
    this.showProductsAction = this.showProductsAction.bind(this);
    this.goBackAction = this.goBackAction.bind(this);
    this.productSelectedAction = this.productSelectedAction.bind(this);
    this.orderAction = this.orderAction.bind(this);
    this.closeDeliveryAction = this.closeDeliveryAction.bind(this);
    this.clearAllAction = this.clearAllAction.bind(this);
  }

  addItemAction() {
  	this.setState({state: 'add', canClear: true});
    this.listComponent.newInput.focus();
    

  }

  componentDidMount() {
    if (JSON.parse(localStorage.getItem('suggest')) === 'hide') {
      this.setState({suggest: false});
    }

    var localItems = JSON.parse(localStorage.getItem('items'));

    if (localItems) {
      this.setState({items: localItems});

      if (localItems.length) {
        this.setState({canClear: true});
        
        this.setState({state: 'list'});

        if (localItems.length > 5) {
          this.setState({canOrder: true});
        }

      }




      
    }
  }



  hideSuggestAction() {
  	this.setState({suggest: false});
    localStorage.setItem('suggest',JSON.stringify('hide'));

  }

  hideNewItem() {
  	this.setState({state: 'list', canOrder: this.listComponent.state.items.length > 5});
  }

  showMenuAction() {
    this.setState({state: 'menu'});
  }

  closeMenuAction() {
    this.setState({state: 'list'});
  }

  clearCheckedAction() {
    this.listComponent.state.items = [];
    localStorage.setItem('items',JSON.stringify([]));
    this.setState({state: 'onboarding', canClear: false, canOrder: false});
  }

  editStartAction() {
    this.setState({state: 'edit'});
  }

  editStopAction() {
    this.setState({state: 'list'});
  }

  showProductsAction() {
    var items = this.listComponent.state.items;
    var filtered = items.filter((item, i)=> {
      return !item.checked;
    });
    this.setState({items: filtered});
    this.setState({page: 'products', canClear: false});
  }

  goBackAction() {
    if (this.state.page === 'products') {
      this.setState({
        page: 'list', 
        productsSelected: [],
        selectedPrice: 0
      });

      if (this.state.items.length > 0) {
        this.setState({canClear: true});
      }
    }
  }

  productSelectedAction(selected, totalPrice) {
    this.setState({productsSelected: selected, selectedPrice: totalPrice});
  }

  orderAction() {
    this.setState({state: 'delivery'});
  }

  closeDeliveryAction() {
    this.setState({state: 'list'});
  }

  clearAllAction() {
    this.setState({
      state: 'list',
      page: 'list',
      suggest: false,
      canClear: false,
      canOrder: false,
      items: [],
      productsSelected: [],
      selectedPrice: 0,
    });
    this.clearCheckedAction();
  }

  render() {
  	var stateClass = 'App--'+this.state.state;
    return (
      <div className={`App ${stateClass} `}>
        <BrowserRouter>
    			<Switch>
    				<Route exact path="/">
    					<div>
    						<Header
                  app={this}
                  showMenuAction={this.showMenuAction}
                  goBackAction={this.goBackAction}
                />
    						<List
    							app={this}
    							hideSuggestAction={this.hideSuggestAction}
    							hideNewItem={this.hideNewItem}
                  ref={(item) => { this.listComponent = item; }}
                  editStartAction={this.editStartAction}
                  editStopAction={this.editStopAction}

    						/>


                <ProductsContainer
                  app={this}
                  
                />
                <Delivery
                  app={this}
                  closeDeliveryAction={this.closeDeliveryAction}
                  clearAllAction={this.clearAllAction}
                  
                />
    						<Footer
    							app={this}
    							addItemAction={this.addItemAction}
                  canOrder={this.state.canOrder}
                  showProductsAction={this.showProductsAction}
                  productsSelected={this.state.productsSelected.length}
                  selectedPrice={this.state.selectedPrice}
                  orderAction={this.orderAction}

    						/>
                <Menu
                  app={this}
                  clearCheckedAction={this.clearCheckedAction}
                  closeMenuAction={this.closeMenuAction}
                  canClear={this.state.canClear}
                  
                />

    					</div>
    				</Route>
    				
    			</Switch>
    		</BrowserRouter>
      </div>
      );
  }
}

export default App;