import React from 'react';

import './style.scss';

export default class extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      items: [
        {link:'http://t-do.ru/provorov', text:'Чат с поддержкой'},
        {link:'http://korobka.me/', text:'О проекте'},
      ]
    };
    this.close = this.close.bind(this);
    this.clear = this.clear.bind(this);

  }

  close() {
    this.props.closeMenuAction();
  }

  clear() {
    this.props.clearCheckedAction();
  }
  
  render() {
    var menuClass = (this.props.app.state.state === 'menu') ? ' Menu--show ' : '';
    var canClear = (this.props.canClear) ? ' Menu--clear ' : '';
    return (
    	<section className={`Menu ${menuClass} ${canClear}`}>
    		<div className="container">
    			<div className="Menu__items">
            <a href="#" onClick={this.clear} className="Menu__item Menu__item--clear">Очистить список</a>
            {
              this.state.items.map((item, i)=>{
                return (<a href={item.link} key={i} target="_blank" className="Menu__item">{item.text}</a>);
              })
            }

          </div>
          <div className="Menu__close" onClick={this.close}>Закрыть</div>
    		</div>
    	</section>
      
      );
  }
}

