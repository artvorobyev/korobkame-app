import React from 'react';

import './style.scss';

export default class extends React.Component{
  constructor(props){
    super(props);
    this.state = {};
    this.showMenu = this.showMenu.bind(this);
    this.goBack = this.goBack.bind(this);
  }

  showMenu() {
  	this.props.showMenuAction();
  }

  goBack() {
    this.props.goBackAction();
  }
  
  render() {
    var backClass = (this.props.app.state.page === 'products') ? ' Header__back--show ' : '';

    return (
    	<section className="Header">
    		<div className="container">
          <div className="Header__top">
            <div className="Header__left">
              <div className={`Header__back ${backClass}`} onClick={this.goBack}>Назад</div>
            </div>
            <div className="Header__right">
              <div className="Header__button" onClick={this.showMenu}></div>
            </div>
            
            
          </div>
    			
    			<h1 className="Header__heading">Что купить</h1>
    		</div>
    	</section>
      
      );
  }
}

