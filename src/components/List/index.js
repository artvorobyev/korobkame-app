import React from 'react';
import { Input } from 'reactstrap';

import './style.scss';

export default class extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      'items': [],
      suggestions: [
        'Шампунь', 'Туалетная бумага', 'Носки', 'Чай', 'Корм', 'Памперсы'
      ],
      newInput: ''
    };

    this.newInputChange = this.newInputChange.bind(this);
    this.addItem = this.addItem.bind(this);
    this.editItem = this.editItem.bind(this);
    this.inputBlur = this.inputBlur.bind(this);
    this.startEdit = this.startEdit.bind(this);

    this.handleClickOutside = this.handleClickOutside.bind(this);
    this.setWrapperRef = this.setWrapperRef.bind(this);
    
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);

    var localItems = JSON.parse(localStorage.getItem('items'));

    if (localItems) {
      this.setState({items: localItems});


    }
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  handleClickOutside(event) {
    if (this.props.app.state.state === 'add') {
      if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
        this.props.hideNewItem();
      }
    }

  }

  setWrapperRef(node) {
    this.wrapperRef = node;
  }

  suggest(text, event) {
    this.addItem(text);
    this.props.hideSuggestAction();
  }

  newInputChange(event) {
    this.setState({newInput:event.target.value});
  }

  editItem(i, event) {
    var items = this.state.items;
    items[i]['text'] = event.target.value;
    items[i]['slug'] = event.target.value.replace(/ /g, '').toLowerCase();
    this.setState({items: items});
  }

  addItem(text) {
    if (text.length) {
      var items = this.state.items;
      items.unshift({text: text, checked: false, edit: false, slug: text.replace(/ /g, '').toLowerCase()});
      this.setState({items: items});

      this.props.hideNewItem();
      this.props.hideSuggestAction();

      localStorage.setItem('items',JSON.stringify(items));
     
    }

     
  }

  checkItem(i) {
    var items = this.state.items;
    items[i]['checked'] = !items[i]['checked'];
    this.setState({items: items});
    localStorage.setItem('items',JSON.stringify(items));
  }

  inputBlur(event) {
    var text = this.state.newInput;
    this.addItem(text);
    this.setState({newInput:''});
  }

  startEdit(i) {
    this.props.editStartAction();
    var items = this.state.items;
    items[i]['edit'] = !items[i]['edit'];
    this.setState({items: items});

  }

  stopEdit(i) {
    this.props.editStopAction();
    var items = this.state.items;
    items[i]['edit'] = !items[i]['edit'];
    this.setState({items: items});

  }

  
  
  render() {
    var th = this;
    var appState = this.props.app.state;
    var listClass = (appState.page !== 'list') ? ' List--hide ' : '';
    var newClass = (appState.state === 'add') ? ' List__new--show ' : '';
    var suggestClass = (appState.suggest) ? ' List__new__suggest--show ' : '';

    return (
    	<section className={`List ${listClass} `}>
    		<div className="container">
    			<div className={`List__new ${newClass}`} ref={this.setWrapperRef}>
            <div className="List__new__input">
              <div className="List__new__input__check"></div>
              <Input
                className="List__new__input__field" 
                onBlur={th.inputBlur} 
                onChange={th.newInputChange} 
                value={th.state.newInput} 
                placeholder="Введите товар"
                innerRef={(ref)=>{ this.newInput = ref;}}
              />
            </div>
            <div className={`List__new__suggest ${suggestClass} `}>
              {
                this.state.suggestions.map((item, i)=>{
                  return(
                    <div className="List__new__suggest__item" onClick={th.suggest.bind(th,item)} key={i}>
                      {item}
                    </div>
                    )
                })
              }
            </div>

          </div>
          <div className="List__items">
            {
                this.state.items.map((item, i)=>{
                  var checkedClass = (item.checked) ? ' List__item--checked ' : '';
                  var editClass = (item.edit) ? ' List__item--edit ' : '';
                  return(
                    <div className={`List__item ${checkedClass} ${editClass} `} key={i}>
                      <div className="List__item__check" onClick={th.checkItem.bind(th,i)}></div>
                      <Input
                        className="List__item__text" 
                        value={item.text} 
                        onFocus={th.startEdit.bind(th,i)}
                        onBlur={th.stopEdit.bind(th,i)} 
                        onChange={th.editItem.bind(th,i)}
                      />
                      
                    </div>
                    )
                })
              }
          </div>
    		</div>
    	</section>
      
      );
  }
}

