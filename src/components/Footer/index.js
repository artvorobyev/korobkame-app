import React from 'react';

import './style.scss';

export default class extends React.Component{
  constructor(props){
    super(props);
    this.state = {};
    this.showSuggest = this.showSuggest.bind(this);
    this.showProducts = this.showProducts.bind(this);
    this.order = this.order.bind(this);
  }

  showSuggest() {
  	this.props.addItemAction();
  }

  showProducts() {
    this.props.showProductsAction();
  }

  order() {
    this.props.orderAction();
  }
  
  render() {
    var appState = this.props.app.state;
    var helpClass = (appState.state === 'onboarding') ? ' Footer__help--show ' : '';
    var productsClass = (this.props.canOrder) ? ' Footer__products--show ' : '';
    var footerClass = (appState.state === 'menu') ? ' Footer--hide ' : '';
    var addClass = (appState.page !== 'list') ? ' Footer__add--hide ' : '';
    var leftClass = (appState.page !== 'list') ? ' Footer__left--hide ' : '';
    var orderClass = (appState.page !== 'products' || !this.props.productsSelected) ? ' Footer__order--hide ' : '';


    return (
    	<section className={`Footer ${footerClass}`}>
    		<div className="container">
          <div className="Footer__row">
            <div className={`Footer__left ${leftClass} `}>
              <div className={`Footer__help ${helpClass} `}>
                Добавьте всё, что заканчивается
              </div>
              <div className={`Footer__products ${productsClass}`} onClick={this.showProducts}>
                Заказать
              </div>
            </div>
            <div className={`Footer__add ${addClass}`} onClick={this.showSuggest}></div>
            <div className={`Footer__order ${orderClass}`} onClick={this.order}>Заказать за {this.props.selectedPrice} ₽</div>
          </div>
    		</div>
    	</section>
      
      );
  }
}

