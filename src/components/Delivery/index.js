import React from 'react';

import { Input, Button } from 'reactstrap';
import InputMask from 'react-input-mask';

import API from '../API';

import './style.scss';

export default class extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      phone: '',
      loading: false,
      success: false
    };

    this.phoneChange = this.phoneChange.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.sendData = this.sendData.bind(this);
    this.clearAll = this.clearAll.bind(this);
  }

  phoneChange(event) {
    this.setState({phone: event.target.value});
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  handleClickOutside(event) {
    if (this.props.app.state.state === 'delivery') {
      if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
        this.props.closeDeliveryAction();
      }
    }

  }

  setWrapperRef(node) {
    this.wrapperRef = node;
  }

  sendData() {
    this.setState({loading: true});

    fetch( API.url+'orders', {
      method:'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },

      body: JSON.stringify({
        order_type: 'auchan',
        phone: this.state.phone,
        products: this.props.app.state.productsSelected
      })
    })
    .then(response => {
      return response.json();
    })
    .then( data => {
      this.setState({loading:false, success: true});
    });
    
    
  }

  clearAll() {
    this.props.clearAllAction();
  }

  
  
  render() {
    var appState = this.props.app.state;
    var deliveryClass = (appState.state === 'delivery') ? ' Delivery--show ' : '';
    var successClass = (this.state.success) ? ' Delivery--success ' : '';
    var buttonLoadingClass = (this.state.loading) ? ' Delivery__button--loading ' : '';
    var buttonDisabled = this.state.phone.replace(/ /g, '').length !== 12;
    
    return (
    	<section className={`Delivery ${deliveryClass} ${successClass}`} ref={this.setWrapperRef}>
        <div className="container">
          <div className="Delivery__body">
            <div className="Delivery__title">ДОСТАВКА</div>
            <InputMask mask="+7 999 999 99 99" maskChar=" " disabled={this.state.loading} value={this.state.phone} onChange={this.phoneChange}>
              {(inputProps) => <Input className="Delivery__input"  placeholder="Телефон"/>}
            </InputMask>
            <div className="Delivery__descr">
              Позвоним, чтобы узнать адрес и&nbsp;договориться о&nbsp;времени
            </div>
            <Button className={`Delivery__button ${buttonLoadingClass} `} disabled={buttonDisabled} onClick={this.sendData}>Оформить</Button>
          </div>
          <div className="Delivery__success">
            <div className="Delivery__title">СПАСИБО</div>
            <div className="Delivery__thanks">Будет здорово, если расскажете о нас друзьям и&nbsp;близким</div>
            <Button className="Delivery__close" onClick={this.clearAll}>Новый список</Button>
          </div>
        </div>
      </section>
      
      );
  }
}

