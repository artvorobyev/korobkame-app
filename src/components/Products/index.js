import React from 'react';
import { Input } from 'reactstrap';

import API from '../API';

import './style.scss';

export default class extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      loading: true,
      items: this.props.items,
      selected: false
    };

    this.toggleItem = this.toggleItem.bind(this);
    this.selectProduct = this.selectProduct.bind(this);
    this.countSelected = this.countSelected.bind(this);
    
  }

  componentDidMount() {
    var array = [];
    this.state.items.map((item)=>{
      array.push(item.slug);
    });
    fetch( API.url+'search?q='+array.join(','))
    .then(response => {
      return response.json();
    })
    .then( data => {
      console.log(data);
      for (var name in data) {
        this.state.items.map((item, i)=>{ 
          if (item.slug === name) {
            this.state.items[i].products = data[name];
          }
        });
        
      }
      this.setState({loading: false});
    });
  }


  toggleItem(i) {
    var items = this.state.items;
    items[i].removed = !items[i].removed;
    if (items[i].removed) {
      items[i].products.map((product, p)=>{
        items[i].products[p].selected = false;
      });
    }
    this.setState({items: items});

    this.countSelected();
  }

  selectProduct(item, product) {
    var items = this.state.items;
    items[item].products.map((p, o)=>{
      if (o !== product)
      p.selected = false;
    });
    items[item].products[product].selected = !items[item].products[product].selected;
    this.setState({items: items});

    this.countSelected();
  }

  countSelected() {
    var selected = [];
    var summ = 0;
    this.state.items.map((item)=>{
      item.products.map((product)=>{
        if (product.selected) {
          selected.push(product);
          summ+=product.price;
        }
      });
    });
    this.props.productSelectedAction(selected, summ);
  }

  
  render() {
    var th = this;
    var productsClass = (this.props.app.state.page !== 'products') ? ' Products--hide ' : '';
    var productsLoadingClass = (this.state.loading) ? ' Products--loading ' : '';

    if (!this.state.loading)
    return (
    	<section className={`Products ${productsClass} ${productsLoadingClass} `}>
    		<div className="container">
    			{
            this.state.items.map((item, i)=>{
              var itemRemovedClass = (item.removed) ? ' Products__item--removed ' : '';
              return(
                <div className={`Products__item ${itemRemovedClass} `} key={i}>
                  <div className="Products__item__top">
                    <div className="Products__item__toggle" onClick={th.toggleItem.bind(th,i,0)}></div>
                    <div className="Products__item__title">{item.text}</div>
                  </div>
                  <div className="Products__item__slider">
                    <div className="Products__item__row">
                      {item.products.map((slide, s)=>{
                        var slideClass = (slide.selected) ? ' Products__item__slide--selected ' : '';
                        return(
                          <div key={i+'_'+s} className={`Products__item__slide ${slideClass}`} onClick={th.selectProduct.bind(th,i,s)}>
                            <div className="Products__item__slide__image">
                              <img src={slide.image_url} onError={(e)=>{e.target.onerror = null; e.target.src="http://korobka.me/fallback.png"}}/>
                                
                              
                            </div>
                            <div className="Products__item__slide__title">{slide.brand}</div>
                            <div className="Products__item__slide__price">{slide.price} ₽</div>
                          </div>
                          )
                      })}
                      
                      
                    </div>
                  </div>
                  
                </div>
                )
            })
          }
    		</div>
    	</section>
      
      );
    else return(
      <section className={`Products ${productsClass} ${productsLoadingClass} `}></section>
      );
  }
}

